require 'rails_helper'

RSpec.feature 'Potepan::ProductsShows', type: :feature do
  feature '商品詳細ページを表示する' do
    let!(:taxonomy) { create :taxonomy }
    let!(:taxon) { create :taxon, taxonomy: taxonomy }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_product) { create(:product, name: 'related_product', price: '11.11', taxons: [taxon]) }

    background do
      visit potepan_product_path product.id
    end

    scenario '商品詳細ページに、商品名、価格、商品説明が表示される' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end

    scenario '関連商品が表示されている' do
      within('.productBox') do
        expect(page).to have_content related_product.name
        expect(page).to have_content related_product.display_price
      end
    end

    scenario '関連商品に、メインの商品は表示されない' do
      within('.productBox') do
        expect(page).not_to have_text product.name
        expect(page).not_to have_text product.display_price
      end
    end
  end
end
