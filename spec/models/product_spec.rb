require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  describe '関連商品が正しく取得できること' do
    let(:taxonomy) { create :taxonomy }
    let(:taxon) { create :taxon }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      create_list(:product, 5, taxons: [taxon])
    end

    it '関連商品にメイン商品が存在しないこと' do
      expect(product.related_products).not_to eq product.name
      expect(product.related_products).not_to eq product.display_price
    end
  end
end
