require 'rails_helper'
require 'spree/testing_support/factories'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'Get request' do
    let!(:taxon) { create :taxon }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

    before do
      get :show, params: { id: product.id }
    end

    it 'assigns @product' do
      expect(assigns(:product)).to eq product
    end

    it 'リクエストが成功すること' do
      expect(response.status).to eq 200
    end

    it 'showテンプレートが表示されること' do
      expect(response).to render_template :show
    end

    context '関連商品が存在する時' do
      it 'assigns @related_products' do
        expect(assigns(:related_products)).to match_array(related_products)
      end
    end

    context '関連商品が4つ以上存在する時' do
      let!(:related_products) { create_list(:product, 10, taxons: [taxon]) }

      it 'assigns @related_products_size' do
        expect(assigns(:related_products).size).to eq 4
      end
    end
  end
end
