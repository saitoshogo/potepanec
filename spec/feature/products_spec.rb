require 'rails_helper'

RSpec.feature 'Potepan::ProductsShows', type: :feature do
  feature '商品詳細ページを表示する' do
    let!(:taxonomy) { create :taxonomy }
    let!(:taxon) { create :taxon, taxonomy: taxonomy }
    let!(:product) { create(:product, taxons: [taxon]) }
    let!(:related_product) { create(:product, name: 'related_product', price: '11.11', taxons: [taxon]) }

    background do
      visit potepan_product_path product.id
    end

    scenario '関連商品クリック時、関連商品の表示が切り替わる' do
      within('.productsContent') do
        click_on related_product.name
        expect(page).to have_http_status(:success)
        expect(page).to have_text product.name
        expect(page).to have_text product.display_price
      end
    end

    scenario '一覧ページへ戻るボタンクリック時、リンク先に遷移' do
      find_link('一覧ページへ戻る').click
      expect(page).to have_http_status(:success)
      expect(current_path).to eq potepan_category_path(taxon.id)
    end

    scenario "light_sectionのHomeボタンクリック時にトップページに遷移する" do
      within('.pageHeader') do
        find_link('Home').click
        expect(page).to have_http_status(:success)
      end
    end

    scenario "headerのHomeボタンクリック時にトップページに遷移する" do
      within('.header') do
        find_link('Home').click
        expect(page).to have_http_status(:success)
      end
    end

    scenario 'headerのロゴをクリック時にトップページに遷移する' do
      page.first('.navbar-brand').click
      expect(page).to have_http_status(:success)
    end
  end
end
