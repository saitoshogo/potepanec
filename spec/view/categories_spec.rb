require 'rails_helper'

RSpec.feature "Categories_feature", type: :feature do
  feature "商品カテゴリ一覧ページを表示する" do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:other_taxonomy) { create(:taxonomy) }
    let!(:taxon) { create(:taxon, name: 'taxon', parent: taxonomy.root) }
    let!(:other_taxon) { create(:taxon, name: 'other_taxon', parent: other_taxonomy.root) }
    let!(:product) { create(:product, name: 'product', price: '10.00', taxons: [taxon]) }
    let!(:other_product) { create(:product, name: 'other_product', price: '20.00', taxons: [other_taxon]) }

    background do
      visit potepan_category_path taxon.id
    end

    scenario '商品カテゴリ一覧ページ遷移後、カテゴリー名、商品名、価格、が表示される' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content taxonomy.name
      within('.productBox') do
        expect(page).not_to have_content other_product.name
        expect(page).not_to have_content other_product.price
      end
    end
  end
end
