class Potepan::ProductsController < ApplicationController
  PRODUCT_DISPLAY_MAX_COUNT = 4
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.sample(PRODUCT_DISPLAY_MAX_COUNT)
  end
end
