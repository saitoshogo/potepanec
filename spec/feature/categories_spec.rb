require 'rails_helper'

RSpec.feature "Categories_feature", type: :feature do
  feature "商品カテゴリ一覧ページを表示する" do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:other_taxonomy) { create(:taxonomy) }
    let!(:taxon) { create(:taxon, name: 'taxon', parent: taxonomy.root) }
    let!(:other_taxon) { create(:taxon, name: 'other_taxon', parent: other_taxonomy.root) }
    let!(:product) { create(:product, name: 'product', price: '10.00', taxons: [taxon]) }
    let!(:other_product) { create(:product, name: 'other_product', price: '20.00', taxons: [other_taxon]) }

    background do
      visit potepan_category_path taxon.id
    end

    scenario 'カテゴリクリック時に画面が切り替わる' do
      click_on other_taxon.name
      expect(page).to have_http_status(:success)
      expect(current_path).to eq potepan_category_path(other_taxon.id)
      expect(page).to have_content other_product.name
      expect(page).to have_content other_product.display_price
    end

    scenario '商品カテゴリ一覧ページの商品名をクリック時に商品詳細ページへ遷移する' do
      click_on product.name
      expect(page).to have_http_status(:success)
    end

    scenario 'light_sectionのHomeボタンクリック時にトップページに遷移する' do
      within('.pageHeader') do
        click_link('Home')
        expect(page).to have_http_status(:success)
      end
    end

    scenario 'headerのHomeボタンクリック時にトップページに遷移する' do
      within('.header') do
        find_link('Home').click
        expect(page).to have_http_status(:success)
      end
    end

    scenario 'headerのロゴをクリック時にトップページに遷移する' do
      page.first('.navbar-brand').click
      expect(page).to have_http_status(:success)
    end
  end
end
